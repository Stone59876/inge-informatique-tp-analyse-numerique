import math

import numpy as np


#  Need A matrix of Ax = b system
#  Return LU decomposition into two matrices L and U
def decompositionLU(A):
    L = np.zeros_like(A)
    U = np.zeros_like(A)
    n = len(A)

    # Matrix must be square to admit a LU decomposition
    if n != len(A[0]):
        raise 'Matrix A is not square'

    for k in range(n):
        L[k, k] = 1
        #  The matrix pivots should not be too small to admit a LU decomposition
        if abs(A[k, k] - np.dot(L[k, :k], U[:k, k])) < 10 ** (-10):
            raise 'A pivot is too small'
        U[k, k] = A[k, k] - np.dot(L[k, :k], U[:k, k])

        for j in range(k+1, n):
            U[k, j] = A[k, j] - np.dot(L[k, :k], U[:k, j])

        for i in range(k+1, n):
            L[i, k] = (A[i, k] - np.dot(L[i, :k], U[:k, k])) / U[k, k]

    if not verifyLUvalidity(A, L, U):
        raise 'Bad LU decomposition'

    return L, U

#  Need A matrix, and it's decomposition composed of L and U matrices
#  Return
#  - True if LU decomposition is valid
#  - False if LU decomposition isn't valid
def verifyLUvalidity(A, L, U):
    n = len(A)
    LU = np.dot(L, U)

    for i in range(n):
        for j in range(n):
            if abs(A[i, j] - LU[i, j]) > 10 ** (-10):
                return False

    return True

#  Need A matrix of Ax = b system
#  Return LU decomposition into one matrix C (concatenation of the matrices L and U in a vertical way)
def factoLU(A):
    L, U = decompositionLU(A)
    return np.concatenate((L, U))


#  Need
#  - C matrix which is the concatenation of the matrices L and U in a vertical way (LU decomposition of A)
#  - b matrix of Ax = b system
#  Return x solution of Ax = b system by using LU decomposition
def drLU(C, b):
    n = int(len(C) / 2)

    #  Degree of the matrix C must be equal to the height of the matrix b
    if n != len(b):
        raise 'Incompatible parameters'

    L = C[0:n, :]
    U = C[n:, :]

    # Down
    Y = np.linalg.solve(L, b)  # LY = b

    # Up
    X = np.linalg.solve(U, Y)  # Y = UX <=> UX = Y

    return X


#  Need A and b matrices of system Ax = b
#  Return x solution of Ax = b system by using LU decomposition
def solveLU(A, b):
    LU = factoLU(A)
    return drLU(LU, b)







def questionB():
    A = np.array([
        [-math.cos(math.pi / 6), 0, math.cos(math.pi/4), 1, 0, 0, 0, 0, 0, 0, 0],  # T1
        [-math.sin(math.pi/6), 0, -math.sin(math.pi/4), 0, 0, 0, 0, 0, 0, 0, 0],  # T2
        [0, -1, -math.cos(math.pi/4), 0, math.cos(math.pi/4), 1, 0, 0, 0, 0, 0],  # T3
        [0, 0, math.sin(math.pi/4), 0, math.sin(math.pi/4), 0, 0, 0, 0, 0, 0],  # T4
        [0, 0, 0, -1, -math.cos(math.pi/4), 0, math.cos(math.pi/4), 1, 0, 0, 0],  # T5
        [0, 0, 0, 0, -math.sin(math.pi/4), 0, -math.sin(math.pi/4), 0, 0, 0, 0],  # T6
        [0, 0, 0, 0, 0, -1, -math.cos(math.pi/4), 0, math.cos(math.pi/4), 1, 0],  # T7
        [0, 0, 0, 0, 0, 0, math.sin(math.pi/6), 0, math.sin(math.pi/4), 0, 0],  # T8
        [0, 0, 0, 0, 0, 0, 0, -1, -math.cos(math.pi/4), 0, math.cos(math.pi/6)],  # T9
        [0, 0, 0, 0, 0, 0, 0, 0, -math.sin(math.pi/4), 0, -math.sin(math.pi/6)],  # T10
        [0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -math.cos(math.pi/6)]  # T11
    ])

    b = np.array([[0],
                  [0],
                  [0],
                  [10000],
                  [0],
                  [0],
                  [0],
                  [20000],
                  [0],
                  [0],
                  [0]])

    return A, b


def questionC():
    A = np.array([
        [-math.sin(math.pi / 6), 0, -math.sin(math.pi / 4), 0, 0, 0, 0, 0, 0, 0, 0],  # T2
        [0, -1, -math.cos(math.pi / 4), 0, math.cos(math.pi / 4), 1, 0, 0, 0, 0, 0],  # T3
        [0, 0, math.sin(math.pi / 4), 0, math.sin(math.pi / 4), 0, 0, 0, 0, 0, 0],  # T4
        [0, 0, 0, -1, -math.cos(math.pi / 4), 0, math.cos(math.pi / 4), 1, 0, 0, 0],  # T5
        [0, 0, 0, 0, -math.sin(math.pi / 4), 0, -math.sin(math.pi / 4), 0, 0, 0, 0],  # T6
        [0, 0, 0, 0, 0, -1, -math.cos(math.pi / 4), 0, math.cos(math.pi / 4), 1, 0],  # T7
        [0, 0, 0, 0, 0, 0, math.sin(math.pi / 6), 0, math.sin(math.pi / 4), 0, 0],  # T8
        [0, 0, 0, 0, 0, 0, 0, -1, -math.cos(math.pi / 4), 0, math.cos(math.pi / 6)],  # T9
        [0, 0, 0, 0, 0, 0, 0, 0, -math.sin(math.pi / 4), 0, -math.sin(math.pi / 6)],  # T10
        [0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -math.cos(math.pi / 6)],  # T11
        [-math.cos(math.pi / 6), 0, math.cos(math.pi / 4), 1, 0, 0, 0, 0, 0, 0, 0]  # T1
    ])

    b = np.array([[0],
                  [0],
                  [10000],
                  [0],
                  [0],
                  [0],
                  [20000],
                  [0],
                  [0],
                  [0],
                  [0]])

    return A, b


def questionF():
    A = np.array([
        [-math.sin(math.pi / 6), 0, -math.sin(math.pi / 4), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # T2
        [0, -1, -math.cos(math.pi / 4), 0, math.cos(math.pi / 4), 1, 0, 0, 0, 0, 0, 0, 0],  # T3
        [0, 0, math.sin(math.pi / 4), 0, math.sin(math.pi / 4), 0, 0, 0, 0, 0, 0, 0, 0],  # T4
        [0, 0, 0, -1, -math.cos(math.pi / 4), 0, math.cos(math.pi / 4), 1, 0, 0, 0, 0, 0],  # T5
        [0, 0, 0, 0, -math.sin(math.pi / 4), 0, -math.sin(math.pi / 4), 0, 0, 0, 0, 0, 0],  # T6
        [0, 0, 0, 0, 0, -1, -math.cos(math.pi / 4), 0, math.cos(math.pi / 4), 1, 0, 0, 0],  # T7
        [0, 0, 0, 0, 0, 0, math.sin(math.pi / 6), 0, math.sin(math.pi / 4), 0, 0, 0, 0],  # T8
        [0, 0, 0, 0, 0, 0, 0, -1, -math.cos(math.pi / 4), 0, math.cos(math.pi / 6), 0, 0],  # T9
        [0, 0, 0, 0, 0, 0, 0, 0, -math.sin(math.pi / 4), 0, -math.sin(math.pi / 6), 0, 0],  # T10
        [0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -math.cos(math.pi / 6), 0, 0],  # T11
        [-math.cos(math.pi / 6), 0, math.cos(math.pi / 4), 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # T1
        [math.sin(math.pi / 6), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  # V1
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, math.sin(math.pi / 6), 0, 1]  # V2
    ])

    b = np.array([[0],
                  [0],
                  [10000],
                  [0],
                  [0],
                  [0],
                  [20000],
                  [0],
                  [0],
                  [0],
                  [0],
                  [0],
                  [0]])

    return A, b




if __name__ == '__main__':
    # A = np.array([[2, -1, -2],
    #               [-4, 6, 3],
    #               [-4, -2, 8]])
    #
    # b = np.array([[1],
    #               [2],
    #               [3]])
    #
    # print("A : ")
    # print(A)
    #
    # print("b : ")
    # print(b)
    #
    # print(solveLU(A, b))

    #  ERROR A principal minor of matrix is zero
    #A, b = questionB()

    #  Put first line in last
    #A, b = questionC()

    #  Add V1 and V2
    A, b = questionF()

    T = solveLU(A, b)

    print(T)

    for i in range(len(T)):
        print("ligne " + str(i+1) + " : ", end="")
        print("membrure en compression" if T[i] < 0 else "membrure en tension")
