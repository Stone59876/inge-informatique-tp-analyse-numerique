import math
import matplotlib.pyplot as plt
import numpy as np


def diffdiv(xi: list, yi: list):
    n = len(xi)
    # Create newton tree
    newton_tree = [xi, yi]  # Initialize Newton tree with two first lines : xi and yi
    for line_index in range(2, n+1):
        line = []
        for column_index in range(n - line_index + 1):
            # (extrémité proche haute - extrémité proche basse)
            upper_part = newton_tree[line_index-1][column_index+1] - newton_tree[line_index-1][column_index]
            # (extrémité loin haute - extrémité loin basse)
            bottom_part = newton_tree[0][line_index+column_index-1] - newton_tree[0][column_index]
            # (upper_part / bottom_part)
            line.append(upper_part / bottom_part)
        newton_tree.append(line)

    # Get A factors
    a_list = []
    for a_index in range(1, len(newton_tree)):
        a_list.append(newton_tree[a_index][0])

    return a_list


def myhorner(dd: list, xi: list, x: list):
    y = []
    for current_x in x:
        current_y = 0
        # Add all a_i(x - x_0)...(x - x_(i-1)) to y
        for a_index in range(0, len(dd)):
            # Init part of y with a_i
            y_part = dd[a_index]
            # Add all (x - x_(0...i-1)) to part of y
            for x_index in range(0, a_index):
                # Add (x - n_i) to part of y
                y_part *= (current_x - xi[x_index])
            # Add part of y to y
            current_y += y_part
        y.append(current_y)
    return y


def fonction(x):
    return x + 1


def fonction1(x):
    return (math.sin(2*math.pi*x)) / (2*math.pi*x)


def fonction2(x):
    return 1 / (1 + pow(x, 2))


def comparaison(f, a, b, n):
    # Define interpolation polynomials

    # Calculate interpolation points antecedent
    xi1 = []
    xi2 = []
    for i in range(1, n+1):  # 1 <= i <= n
        xi1_value = a + (i - 1) * ((b - a) / (n - 1))
        xi2_value = (((a + b) / 2) * a) + (((b - a) / 2) * math.cos(((2 * i) - 1) * (math.pi / (2 * n))))
        xi1.append(xi1_value)
        xi2.append(xi2_value)

    # Calculate interpolation points image
    yi1 = []
    yi2 = []
    for i in range(n):
        yi1.append(f(xi1[i]))
        yi2.append(f(xi2[i]))

    # Calculate factors of Newton interpolation polynomial
    dd1 = diffdiv(xi1, yi1)
    dd2 = diffdiv(xi2, yi2)



    # Define values with function and interpolation polynomials

    # Calculate points antecedent (100n points for smoother graphics)
    x = np.arange(a, b, ((b+(b-a)/n) - a) / (n*100))
    x[-1] = b

    # Calculate function images
    points_function = []
    for current_x in x:
        points_function.append(f(current_x))

    # Calculate Newton interpolation images (with equally spaced interpolation points)
    points_interpolation_1 = myhorner(dd1, xi1, x)

    # Calculate Newton interpolation images (with Tchebychev interpolation points)
    points_interpolation_2 = myhorner(dd2, xi2, x)



    # Draw results

    # Interpolation points
    plt.scatter(xi1, yi1)
    plt.scatter(xi2, yi2)

    # Interpolation polynomials
    plt.plot(x, points_interpolation_1)
    plt.plot(x, points_interpolation_2)

    # Function
    plt.plot(x, points_function)

    plt.legend([
        "Points d'interpolation équirépartis",
        "Points d'interpolation de Tchebychev",
        "Interpolation équirépartie",
        "Interpolation de Tchebychev",
        "f(x)"])
    plt.show()


if __name__ == '__main__':
    xi = [-1, 0, 2, 5]
    yi = [-2, 2, 4, 1]
    dd = diffdiv(xi, yi)
    x = [-1, 0, 2, 5]
    print(myhorner(dd, xi, x))
    # comparaison(fonction, 1, 100, 100)
    comparaison(fonction1, -2, 2, 10)
    comparaison(fonction2, -5, 5, 10)
