import math
import matplotlib.pyplot as plt

from exercice2 import diffdiv


def f(x):
    return math.sqrt(1+x)

def P(x):
    return (2 - 2*math.sqrt(6) + 2*math.sqrt(2)) * pow(x, 2) + (-3 + 2*math.sqrt(6) - math.sqrt(2)) * x + 1


def polynomial(function, N):
    result = []
    for i in range(0, N+1):
        x = i / N
        result.append(function(x))
    return result

def difference(function1, function2, N, is_abs=True):
    result = []
    for i in range(0, N+1):
        x = i / N
        if is_abs:
            result.append(abs(function1(x) - function2(x)))
        else:
            result.append(function1(x) - function2(x))
    return result

if __name__ == '__main__':

    points_f = polynomial(f, 100)

    points_P = polynomial(P, 100)

    plt.plot(points_f)
    plt.plot(points_P)
    plt.legend(["f(x)", "P(x)"])
    plt.show()

    points_error = difference(f, P, 100, isAbs=False)

    plt.plot(points_error)
    plt.legend(["f(x) - P(x)"])
    plt.show()

    points_difference = difference(f, P, 100, isAbs=True)

    plt.plot(points_difference)
    plt.legend(["|f(x) - P(x)|"])
    plt.show()
