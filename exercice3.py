import math
import matplotlib.pyplot as plt
import numpy as np


def calc_a(a, b, f, n):
    coefa = []
    h = (b - a) / n
    for i in range(0, n + 1):
        xi = a + i * h
        xi1 = a + (i + 1) * h
        xi2 = a + (i + 2) * h
        fxi = f(xi)
        fxi1 = f(xi1)
        fxi2 = f(xi2)
        g = (fxi1 - fxi) / (6 * pow(h, 3))
        d = (fxi1 - fxi2) / (6 * pow(h, 3))
        ai = g - d
        coefa.append(ai)
    return coefa


def calc_bc(a, b, f, n, coefa):
    coefb = []
    coefc = []
    h = (b - a) / n
    for i in range(0, n):
        xi = a + i * h
        xi1 = a + (i + 1) * h
        fxi = f(xi)
        fxi1 = f(xi1)
        deltai = fxi1 - fxi
        bi = deltai / h - ((coefa[i + 1] - coefa[i]) * pow(h, 2))
        coefb.append(bi)
        ci = fxi - (coefa[i] * pow(h, 3)) - ((deltai / h) * xi) + ((coefa[i + 1] - coefa[i]) * pow(h, 2) * xi)
        coefc.append(ci)
    coef = [coefb, coefc]
    return coef


def spline_nat(a, b, f, n):
    # Calculate interpolation

    h = (b - a) / n
    listexi = []
    listefxi = []
    liste_sxi = []
    coefa = calc_a(a, b, f, n)
    coef = calc_bc(a, b, f, n, coefa)
    coefb = coef[0]
    coefc = coef[1]
    for i in range(0, n):
        xi = a + i * h
        xi1 = a + (i + 1) * h
        listexi.append(xi)
        listefxi.append(f(xi))
        for x in np.arange(xi, xi1):
            si = (coefa[i + 1] * pow((x - xi), 3)) + (coefa[i] * pow((xi1 - x), 3)) + (coefb[i] * x) + coefc[i]
            liste_sxi.append(si)



    # Calculate 100n points of initial function

    # Calculate points antecedent (100n points for smoother graphics)
    x = np.arange(a, b, ((b + (b - a) / n) - a) / (n * 100))

    # Calculate function images
    points_function = []
    for current_x in x:
        points_function.append(f(current_x))



    # Draw results

    # Interpolation points
    plt.scatter(listexi, listefxi)

    # Interpolation polynomial
    plt.plot(listexi, liste_sxi)

    # Function
    plt.plot(x, points_function)

    plt.legend([
        "Points d'interpolation",
        "S(xi)",
        "f(xi)"])
    plt.show()


def fonction(x):
    return x * 2


def fonction2(x):
    return 1 / (1 + pow(x, 2))


if __name__ == '__main__':
    a = -5
    b = 5
    n = 10
    f = fonction2
    coefa = calc_a(a, b, f, n)
    coef = calc_bc(a, b, f, n, coefa)
    coefb = coef[0]
    coefc = coef[1]
    spline_nat(a, b, f, n)
